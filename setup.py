from setuptools import setup, find_packages


setup(name='control',
      version='0.1',
      license='MIT',
      author='Zefirova Anna',
      author_email='zefirova.am@phystech.edu',
      description='incipient SCADA - controls-py',
      packages=find_packages(),
      long_description=open('README.md').read(),
      #setup_requires=зависимости,
      classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
      ],
      python_requires='>=3.5',
      zip_safe=False)
