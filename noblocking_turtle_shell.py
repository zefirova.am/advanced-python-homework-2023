import cmd
from threading import Thread, Event
from turtle import bye
from queue import Queue, Empty

from equipment.turtle_device import TurtleDevice


class TurtleDeviceThread(Thread):
    def __init__(self):
        super().__init__()
        self.device = TurtleDevice()
        self.queue = Queue()
        
    def run(self):
        while True:
            try:
                item = self.queue.get()
            except self.queue.Empty:
                continue
            else:
                if (item == 'exit'):
                    break
                self.device.execute(*item)
                self.queue.task_done()
        

class NoBlockingTurtleShell(cmd.Cmd):
    intro = 'Welcome to the turtle shell.   Type help or ? to list commands.\n'
    prompt = '(turtle) '
    file = None

    def __init__(self, turtle_thread: TurtleDeviceThread):
        super(NoBlockingTurtleShell, self).__init__()
        self.turtle_thread = turtle_thread
        

    def do_execute(self, arg):
        self.turtle_thread.queue.put(parse(arg))

    def do_exit(self, arg):
        self.close()
        self.turtle_thread.queue.put("exit")
        print('Thank you for using Turtle')
        return True
        
        
    def close(self):
        if self.file:
            self.file.close()
            self.file = None

def parse(arg):
    'Convert a series of zero or more numbers to an argument tuple'
    return tuple(arg.split())

if __name__ == '__main__':
    turtle_thread = TurtleDeviceThread()
    turtle_thread.start()
    NoBlockingTurtleShell(turtle_thread).cmdloop()