# advanced-python-homework-2023
## 1. The sequence of commands to create the virtual enviroment:
```sh
sudo apt install python3.10-venv
cd advanced-python-homework-2023 
python3 -m vemv devenv
source devenv/bin/activate
python3 -m pip install sphinx
python3 -m pip install Pylint
python3 -m pip install MyPy
deactivate
```

## 2. The sequence of commands to create the automated sphinx documentation:
```sh
cd advanced-python-homework-2023
mkdir doc
cd doc
sphinx-quickstart
cd ..
sphinx-apidoc -o doc .
cd doc
rm modules.rst
rm setup.rst
```
Move controls.rst to the source.
```
cd source
vim conf.py
```
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join("..", "..")))

extensions = ['sphinx.ext.autodoc']
```
vim index.rst
```
./controls.rst
```
cd ..
make html
```
Open file://wsl.localhost/Ubuntu/home/zefirka/advanced-python-homework-2023/doc/build/html/index.html 

***Tadam***

## 3. Function running time for different interpreters

|           | CPython3.9 | CPython3.11| PyPy7.3   |
|-----------|------------|------------|-----------|
| without TH| 0.395806057| 0.23031235 |0.099493876|
| with TH   | 0.398283844| 0.223160335|0.109582296|
| with numpy| 0.69133805 | 0.473824731|5.184651649|
| user+sys  | 1.689+0.308| 1.066+0.319|6.215+0.350|

PyPy 5.7 didn't download due to error: '...libffi.so.6: cannot open shared object file: No such file or directory'. It's too hard than requied: I did my homework in WSL.
