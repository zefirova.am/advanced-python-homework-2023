"""
The package 'controls' is intended for SCAD systems (Supervisory Control and Data Acquisition).
Concept of Scada is the automated development of control systems. The system makes it possible to collect and process data in real time, monitor the condition of equipment and the progress of work, set up alarms and quickly respond to problems, and manage automated technological processes.
"""
