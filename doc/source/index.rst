.. SCADA - controls-py documentation master file, created by
   sphinx-quickstart on Thu Sep 28 13:22:10 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SCADA - controls-py's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./controls.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
